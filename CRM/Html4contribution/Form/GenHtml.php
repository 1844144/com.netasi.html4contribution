<?php

require_once 'CRM/Core/Form.php';

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Html4contribution_Form_GenHtml extends CRM_Core_Form {
  function buildQuickForm() {


    $session = CRM_Core_Session::singleton();
    $contributionIds = $session->get("my_ids");
    $contactIds = $session->get("my_contacts");
    $templateId = $session->get("my_message_template");

    $result = civicrm_api3('MessageTemplate', 'get', array(
      'sequential' => 1,
      'id' => $templateId,
    ));

    $html_message = $result['values'][0]['msg_html'];

    

    $messageToken = CRM_Utils_Token::getTokens($html_message);
    $domain = CRM_Core_BAO_Domain::getDomain();

    $outputHtml = array();

    for ($i=0; $i < count($contributionIds); $i += 1) {
      $contact = $contactIds[$i];
      $contribution = $contributionIds[$i];

      // $contact =  civicrm_api3('Contact', 'get', array(
      //   'sequential' => 1,
      //   'id' => $contactIds[$i],
      // ))['values'][0];

      // $contribution =  civicrm_api3('Contact', 'get', array(
      //   'sequential' => 1,
      //   'id' => $contributionIds[$i],
      // ))['values'][0];

      $tokens = array();
      CRM_Utils_Hook::tokens($tokens);
      $tokens = array_merge($tokens, $messageToken);

      $categories = array_keys($tokens);

      $returnProperties = array();
      if (isset($tokens['contact'])) {
        foreach ($tokens['contact'] as $key => $value) {
          if (!isset($returnProperties[$value])) {
            $returnProperties[$value] = 1;
          }
        }
      }

      $params = array('contact_id' => $contact);
      list($contacts) = CRM_Utils_Token::getTokenDetails(
        $params,
        $returnProperties,
        TRUE,
        TRUE,
        NULL,
        $messageToken
      );
      $contact = $contacts[$contact];
      // var_dump($contacts);


      $returnProperties = array();
      if (isset($tokens['contribution'])) {
        foreach ($tokens['contribution'] as $key => $value) {
          if (!isset($returnProperties[$value])) {
            $returnProperties[$value] = 1;
          }
        }
      }



      $contribution_details = CRM_Utils_Token::getContributionTokenDetails(array('contribution_id' => $contribution),
        $returnProperties,
        NULL,
        $messageToken
      );
      $contribution = $contribution_details[$contribution];


      $tokenHtml = CRM_Utils_Token::replaceContactTokens($html_message, $contact, FALSE, $messageToken);

      // replace receipt Number token
      $res = civicrm_api3('ReceiptNumber', 'get', array(
        'sequential' => 1,
        'contribution_id' => $contribution['id'],
      ));
      if ($res['count'] != 0 ){
        $receiptnum = $res['values'][0]['number'];
        $tokenHtml = str_replace("{tax_receipt.number}", $receiptnum, $tokenHtml);
        
      } 

      $tokenHtml = CRM_Utils_Token::replaceDomainTokens($tokenHtml, $domain, TRUE, $categories);
      $tokenHtml = CRM_Utils_Token::replaceContributionTokens($tokenHtml, $contribution, FALSE, $messageToken);

      
      $tokenHtml = CRM_Utils_Token::replaceComponentTokens($tokenHtml, $contact, $categories);
      $tokenHtml = CRM_Utils_Token::replaceHookTokens($tokenHtml, $contact, $categories, FALSE);
      $tokenHtml = CRM_Utils_Token::parseThroughSmarty($tokenHtml, $contact);

      // var_dump($domain);
      $outputHtml[] = $tokenHtml;
      // echo($tokenHtml);
      // var_dump('========================');

    }


    $this->assign('html_array', $outputHtml);

    $this->addButtons(array(
      array(
        'type' => 'submit',
        'name' => ts('Done'),
        'isDefault' => TRUE,
      ),
    ));

    parent::buildQuickForm();
  }

  function preProcess() {
    // // set print view, so that print templates are called
    $this->controller->setPrint(1);
  }

  // function postProcess() {
  //   // $values = $this->exportValues();
  //   // $options = $this->getColorOptions();
  //   // CRM_Core_Session::setStatus(ts('You picked color "%1"', array(
  //     // 1 => $options[$values['favorite_color']]
  //   // )));
  //   parent::postProcess();
  // }


  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
    // items don't have labels.  We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }
}
