<?php

class CRM_Html4contribution_Generate extends CRM_Contribute_Form_Task {

  public $_single = FALSE;

  protected $_rows;

  /**
   * Build all the data structures needed to build the form.
   *
   * @return void
   */
  public function preProcess() {
    // $id = CRM_Utils_Request::retrieve('id', 'Positive',
    //   $this, FALSE
    // );

    // if ($id) {
    //   $this->_contributionIds = array($id);
    //   $this->_componentClause = " civicrm_contribution.id IN ( $id ) ";
    //   $this->_single = TRUE;
    //   $this->assign('totalSelectedContributions', 1);
    // }
    // else {
        parent::preProcess();
    // }

    // // check that all the contribution ids have pending status
    //       $query = "
    //   SELECT count(*)
    //   FROM   civicrm_contribution
    //   WHERE  contribution_status_id != 1
    //   AND    {$this->_componentClause}";
    // $count = CRM_Core_DAO::singleValueQuery($query);
    // if ($count != 0) {
    //   CRM_Core_Error::statusBounce("Please select only online contributions with Completed status.");
    // }

    // // we have all the contribution ids, so now we get the contact ids
    parent::setContactIDs();

    $session = CRM_Core_Session::singleton();
    $ids = $this->_contributionIds;
    // evar_dump("FIIIRST");
    // evar_dump($ids);
    $session->set("my_ids", $ids);
    $session->set("my_contacts", $this->_contactIds);
    // $this->assign('single', $this->_single);

    // $qfKey = CRM_Utils_Request::retrieve('qfKey', 'String', $this);
    // $urlParams = 'force=1';
    // if (CRM_Utils_Rule::qfKey($qfKey)) {
    //   $urlParams .= "&qfKey=$qfKey";
    // }

    // $url = CRM_Utils_System::url('civicrm/contribute/search', $urlParams);
    // $breadCrumb = array(
    //   array(
    //     'url' => $url,
    //     'title' => ts('Search Results'),
    //   ),
    // );

    // CRM_Utils_System::appendBreadCrumb($breadCrumb);
    CRM_Utils_System::setTitle(ts('Generate HTML for Contribution'));
  }

  /**
   * Build the form object.
   *
   *
   * @return void
   */
  public function buildQuickForm() {

    $result = civicrm_api3('MessageTemplate', 'get', array(
      'sequential' => 1,
      'workflow_id' => array('IS NULL' => 1),
    ));

    $templates = array();
    foreach ($result['values'] as $tpl) {
      $templates[ intval($tpl['id'])] = ts($tpl['msg_title']); 
    }



    $this->add('select', 'message_template', ts('Select Template to use: '),
      array(0 => ts('- default -')) + $templates
    );

    $this->addButtons(array(
        array(
          'type' => 'next',
          'name' => ts('Generate'),
          'isDefault' => TRUE,
        ),
        array(
          'type' => 'back',
          'name' => ts('Cancel'),
        ),
      )
    );
  }

  /**
   * Set default values.
   */
  public function setDefaultValues() {
    $defaultFormat = CRM_Core_BAO_PdfFormat::getDefaultValues();
    return array('pdf_format_id' => $defaultFormat['id'], 'receipt_update' => 1, 'override_privacy' => 0);
  }

  /**
   * Process the form after the input has been submitted and validated.
   *
   *
   * @return void
   */
  public function postProcess() {

    $session = CRM_Core_Session::singleton();
    $session->set('my_message_template',$this->_submitValues['message_template']);


    // $this->controller->resetPage('CRM_Html4contribution_Form_GenHtml');
    $session->replaceUserContext(CRM_Utils_System::url('civicrm/genhtml'));
    
    // CRM_Utils_System::redirect(CRM_Utils_System::url("civicrm/genhtml"));
  }
 
  
}