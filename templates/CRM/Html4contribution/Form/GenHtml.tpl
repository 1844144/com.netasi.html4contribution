{* HEADER *}
{literal}
<style>
@media print {
	.print-page{
		page-break-after: always;
		page-break-inside: avoid;
	}
}
</style>
{/literal}
{foreach from=$html_array item=it}
	<div class='print-page'>
		{$it}
	</div>
{/foreach}
{* FOOTER *}
<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>
