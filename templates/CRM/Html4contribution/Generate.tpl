<div class="messages status no-popup">
  <div class="icon inform-icon"></div>
      {include file="CRM/Contribute/Form/Task.tpl"}
</div>
<div id="help">
    {ts}You may choose to email receipts to contributors OR download a PDF file containing one receipt per page to your local computer by clicking <strong>Process Receipt(s)</strong>. Your browser may display the file for you automatically, or you may need to open it for printing using any PDF reader (such as Adobe&reg; Reader).{/ts}
</div>

<table class="form-layout-compressed">
  <tr>
    <td>{$form.output.email_receipt.html}</td>
  </tr>
  <tr>
    <td>{$form.output.pdf_receipt.html}</td>
  </tr>
  <tr id="selectPdfFormat" >
    <td>{$form.message_template.label} {$form.message_template.html}</td>
  </tr>
  <tr>
    <td>{$form.receipt_update.html} {$form.receipt_update.label}</td>
  </tr>
  <tr>
    <td>{$form.override_privacy.html} {$form.override_privacy.label}</td>
  </tr>
</table>

<div class="spacer"></div>
<div class="form-item">
 {$form.buttons.html}
</div>
